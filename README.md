<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Platediagnostics](#platediagnostics)
  - [version2](#version2)
- [Installing](#installing)
- [Running the script](#running-the-script)
  - [The stages of the pipeline](#the-stages-of-the-pipeline)
- [Required input files](#required-input-files)
  - [Log files from the various steps:](#log-files-from-the-various-steps)
  - [Files output by `tally.pl`:](#files-output-by-tallypl)
    - [Main output](#main-output)
- [Exit status and output files](#exit-status-and-output-files)
- [Documentation](#documentation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Platediagnostics

`platediagnostics2` is an R library (and driver script) for quality
control of a scRNA-Seq run on a 384 wells plate using the CEL-Seq2
protocol and the scripts found in the
https://bitbucket.org/princessmaximacenter/scseq repository.

This is a much diverged fork of https://github.com/MauroJM/single-cell-sequencing by
Mauro Muraro (van Oudenaarden group, Hubrecht Institute, Utrecht).

The main script is platediagnostics.R, which is intended to run as an
`Rscript` script (Unix only, for now), and run either stand-alone or
under the control of the Cromwell WDL execution machine.

## version2

When running `Sharq2`, the `platediagostics2` version of the library is required as
there a few minor changes that make it incompatible with the previous version.

# Installing


```
# If the devtools installer is not installed, install it as:

> install.packages("devtools")

#  Install the SCutils package as:

> devtools::install_bitbucket("princessmaximacenter/scutils")

#  Install the platediagostics2 package as:

> devtools::install_bitbucket("princessmaximacenter/platediagnostics2")

# To start using it do

> library(platediagostics2)
```

You also need the prerequisites in the 'Depends:' field of the `DESCRIPTION` file (you will be warned about those
that are not installed). Best install them using `BiocManager`:

```
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
library(BiocManager)
install(SOME_PACKAGE)
```

# Running the script

Do `platediagnostics2.R --help` to see the options. Note that the `--empties` argument must
specify the wells deliberately left empty during sorting. They is used to automatically 
determine the liveness-cutoff, which is the number of biological transcripts above which a
well is assumed to have contained a live cell at the time of sorting. 

The `platediagnostics2.R` script is currently easiest to use in
conjunction with the Cromwell execution environment.  There are quite a
few input files needed (such as log files from earliers stages of the
pipeline), and Cromwell collects their names into a file that can be
used as the argument to the `--inputs` parameter. Alternatively, use
`--prefix` to specify a filename pattern that evaluates to a list of
input files. 


## The stages of the pipeline

To understand what each input file means, here is a brief description of
the stages currently used

 * prepocessing: combine read1 which contains the UMI and well barcode with 
   read2 which contains the mRNA of interest. (WDL task 'combineReads')
 * trimming: apply adapter, homopolynucleotide,
   sequencing quality and complexity trimming (WDL task 'combineReads')
 * mapping: map the trimmed reads onto the genome using the STAR mapper
   (this yields just coordinates, not yet genes). (WDL task 'STAR')
 * tallying: sort reads by well and by gene, calculate abundances, statistics
   and accumulation curves (WDL task 'tally')

# Required input files 

The following input files are needed; they should be listed in the file
given to the --filesFrom`-parameter, or `--prefix=`*name*` should
evaluate to them.

The files are listed 'chronologically'.

## Log files from the various steps:

TODO

## Files output by `tally.pl`:
### Main output
TODO

# Exit status and output files

The exit status is 0 for succes, 1 otherwise. *There is no `stdout` nor
`stderr`*.

The output files (when run non-interactively) are:

TODO


# Documentation

Extensive documentation on the contents and interpretation of the resulting report
can be found in the `doc` subdirectory.

