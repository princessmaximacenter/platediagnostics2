## Functions needed for QC-ing SORT-seq data.
## The main function 'platediagnostics()' is in main.R
## global variables are in config.R
## plotting functions in plot-funcs.R
## reading/writing functions are in io-funcs.R
## functions for fitting Michaelis-Menten model to the saturation curves  is in fitting-funcs.R
## misc functions are in misc-funcs.R

suppressPackageStartupMessages(library(gridExtra))
suppressPackageStartupMessages(library(grid))
suppressPackageStartupMessages(library(gridBase))

platediagnostics <- function(run.info=NULL,
                             args=NULL) {
  if(is.null(getwd()))
     stop("current working directory is NULL (has directory disappeared?)")
  
  inputs <- args$inputs # already a named vector mapping name to filename
  options(stringsAsFactors = FALSE)

  invisible(set.warnings.file(args$outputPrefix)) # used as a sink for warnings()
  
  class.names <- config.class.names()
  stats <- read.stats(file=inputs['classStats'])
  if(!is.null(stats) && !setequal(names(class.names),rownames(stats)))
    stop("Expected rows ",
         paste(collapse=",", names(class.names)),
         " ,\ninstead got ",
         paste(collapse=",", rownames(stats)), "\n")

  count.data <- list()
  for(name in config.count.inputs()) {
    file <- inputs[name]                # may be NA but leave for now
    x <- SCutils::read.counts(file=file)
    if(is.null(x)) {
      warning(file, " containing ",name," data could not be read")
      x <- NA
    }
    count.data[[name]] <- x
  }
  
  wellset.names <- config.wellset.names()
  wellsets <- get.well.sets(args)
  
  last.nreads <- 1000000 # put into args?

  saturations <- read.saturations(inputs,
                                  last.nreads=last.nreads,
                                  percentage=args$accumulation_percentage,
                                  wellsets=wellsets)
  if(is.null(saturations))
    warning("No saturations or too few reads, carrying on")
  else {
    file <- paste0(args$outputPrefix, ".complexities.txt")
    warning("Printing per well complexities to ", file)
    write.tab(file=file,x=perwell.complexities(saturations, last.nreads=last.nreads))
  }
  
  erccs <- count.data[['erccs']]
  if(sum(is.na(erccs))>0)
    stop("No ERCCs read; for now, that's a show stopper, sorry.")
      
  goodERCCs <- goodERCCs(erccs,
                         good.ERCC.median=args$goodERCCs_median,
                         min.req.ERCCs=  args$goodERCCs_minreq,
                         ignore=wellsets$ignore)
  
  n.goodERCCs <- nrow(goodERCCs)
  degraded.mode <- ""
  
  if( n.goodERCCs ==0) {
    msg <- sprintf("Could find no ERCCs (out of %d) having a median (across all wells) of least %d transcripts.
Too many wells failed. Will retry in partially degraded mode now", nrow(erccs), args$goodERCCs_median)
    warning(msg)
    cat(file=stderr(), msg)
    degraded.mode <- "running in partially degraded mode\nfew good ERCCs, bad plate"
    degraded.median <- args$goodERCCs_median/2 -0.1
    goodERCCs <- goodERCCs(erccs,
                           good.ERCC.median=degraded.median,
                           min.req.ERCCs=1,
                           ignore=wellsets$ignore )
    n.goodERCCs <- nrow(goodERCCs)

    if (n.goodERCCs ==0) {
      degraded.mode <- "running in fully degraded mode\nno good ERCCs, AWFUL plate"
      msg <- sprintf("Could STILL not find ERCCs (out of %g) having a median (across all wells) of least %g transcripts.
ALL WELLS FAILED. Will use simply the top 3 ERCCs)", nrow(erccs), degraded.median)
      warning(msg)
      cat(file=stderr(), msg)
      tot <- apply(erccs, 1, sum, na.rm=TRUE)
      top3 <- order(tot, decreasing=TRUE)[1:3]
      goodERCCs <- erccs[ top3, ]
    }
  }
   
  good.wells <- goodwells(goodERCCs, #i.e did reaction work?
                          allowfailed= args$goodwells_allowfailed,
                          allowfailed.frac =args$goodwells_allowfailedfrac,
                          madlog.factor= args$goodwells_madlog,
                          ignore=wellsets$ignore)
  wellsets$good <- good.wells
  wellsets$failed <- with(wellsets, setdiff(all, good))

  wanted <- args$mainMappingType
  genes <- count.data[[wanted]] # @@@ NOTE: 'exons' is like sharq1
  if(sum(is.na(genes))>0)
    stop("No data for requested type '",wanted,"' at all? Here is where I give up ... ")
  
  logTxpts <- log2(colSums(genes)+1)
  
  livecell.cutoff <- livecellcutoff(genes, wellsets)
  min.liveness.cutoff <- args$min_liveness_cutoff
  if(is.na(livecell.cutoff)) {
    degraded.mode <- "Running in degraded mode\nnot enough  good empty wells, don't trust cutoff!!"
    warning(sprintf("Livecellcutoff could not be determined using %d instead", min.liveness.cutoff))
    livecell.cutoff <- min.liveness.cutoff
  }
  exceeding <- names(which(logTxpts>log2(livecell.cutoff)))
  wellsets$live <- with(wellsets, Reduce(intersect, list(good, full, exceeding)))

  ## extend the wellsets
  wellsets <- c(wellsets,
                with(wellsets,
                     list(emptyfailed=intersect(empty,failed),
                          emptygood=intersect(empty,good),
                          fullfailed=intersect(full,failed),
                          fullgood=intersect(full,good))))
  wellsets$dead <- with(wellsets, setdiff(fullgood, live))
  wellset.stats <- wellsetstats(stats, wellsets)

  if(!is.null(wellset.stats)) {
    w <- sum(wellset.stats[,c('all','unk','ignore')])
    
    #@# ntot <- json$tally$total # note: json$tally$allreads includes the invalid_UMIs which may not be in the count tables
    #@# if(is.null(ntot))
    #@#   warning("No logs were given, cannot check if numbers add up")
    #@# else { 
    #@#   if ( w != ntot )
    #@#     stop(sprintf("*** Numbers don't add up: sum of stats table says %d, logfile says %d ***", w, ntot))
    #@# }      

    ## dump tables of stats and live cells
    if(args$debug)
      statsfile <- ""                             #i.e. console
    else {
      statsfile <- paste0(args$outputPrefix, ".stats.txt")
      warning("Printing stat per wellset to ", statsfile)
    }    
    write.table(wellset.stats[names(class.names), names(wellset.names)], #order from most to least useful
                file=statsfile, sep="\t", quote=FALSE, row.names=TRUE, col.names=NA, na="")

    ## write the ERCC/goodwells stats too
    cat(append=TRUE, file=statsfile, sprintf("## good ERCCs species: %d\n",nrow(goodERCCs)))
    cat(append=TRUE, file=statsfile, sprintf("## good ERCCs txpts: %.0f\n",sum(goodERCCs)))
    cat(append=TRUE, file=statsfile, sprintf("## good ERCCs well totals median: %.0f\n",median(colSums(goodERCCs))))
    cat(append=TRUE, file=statsfile, sprintf("## good ERCCs well totals iqr: %.0f\n",IQR(colSums(goodERCCs))))
    cat(append=TRUE, file=statsfile, sprintf("## good wells: %d\n",length(wellsets$good)))
    cat(append=TRUE, file=statsfile, sprintf("## live wells: %d\n",length(wellsets$live)))
    cat(append=TRUE, file=statsfile, sprintf("## pct good wells (out of 384): %.1f %%\n",
                                             100*length(wellsets$good)/384))
    nfullgood <- length(wellsets$fullgood)
    cat(append=TRUE, file=statsfile, sprintf("## pct live wells (out of %d good nonempty)): %.1f %%\n",
                                             nfullgood, 100*length(wellsets$live)/nfullgood))
  } else {
    warning("No rawcounts given, can therefore not write stats file")
  }

#@#   tally <- json$tally
#@#   ## overal numbers:
#@#   numbers <- c(# pre-tally:
#@#                total=json$compTrim$seen, # note: < number of bamlines due to multi-reads
#@#                trimmed=json$compTrim$survive,
#@#                ## bamlines=tally$allreads,
#@#                ## mappings=tally$total,
#@#                ## things that don't fit in next table:
#@#                usable=tally$usable,     # uniquely sense-mapped, not ERCC nor mito
#@#                ## unmapped=tally$unmapped,
#@#                ## unknown_well=tally$unknown_barcodes, 
#@#                rescued_CBC=tally$rescued_barcodes,
#@#                invalid_UMIs=tally$invalidUMIs)
#@# 
#@#   for (name in c('saturated_UMIs', 'exhausted_UMIs')) { # old and new name, get rid of later
#@#     exh <- tally[[name]]
#@#     if(!is.null(exh))
#@#       numbers <- c(numbers, exhausted_UMIs=exh)
#@#   }
#@#   
  
  ## ---- first all calculations (so we can rearrange all plots at will)
  logERCCs <- log2(colMeans(goodERCCs)+1)
  
  if(args$debug) {
    livefile <- paste0(args$outputPrefix,".live.txt")
    warning("Printing live cell data to ", livefile, " (sorted by decreasing summed expression)")
    tab <- genes[  order(rowSums(genes), decreasing=TRUE),  wellsets$live, drop=FALSE ]
    tab <- tab[ rowSums(tab)>0,]
    write.table(format(tab,digits=2, scientific=FALSE),
                file=livefile, sep="\t", quote=FALSE, row.names=TRUE, col.names=NA, na="")
  }

  ## ---- lastly, all the plotting. General plotting related stuff:
  if (interactive() && !args$debug ) {
    warning("Interactive debugging mode, not creating PDF but firing up X11 window")
    pdf <- NA
  } else {
    pdf <- paste0(args$outputPrefix,".platediagnostics.pdf")
    warning("Creating file ", pdf, "\n")
    A4.width <- 8.3
    A4.height <- 11.7
    pdf(file=pdf, width=A4.height, height=A4.width) # landscape. config?
    ## cairo_pdf: needed for unicode, but requires explicit the newpage commands or so!!
    par(mfrow = c(3,4)) # specify grid for plots on the pdf
  } 

  style <- list(cols = c(good="black", failed="red"),
                pchs = c(full=19, empty=21),
                bgs = c(full='black', empty='white'),
                cexs = c(full=0.7, empty=1))

  to.plot <- args$plot.functions # boolean vector of functions to plot
  
  call.if.wanted('stats.plot', to.plot,
                 wellset.stats, wellsets, class.names, wellset.names)
  
  ## differential plotting of full vs. empty, and good vs. failed wells
  data <- (if (sum(is.na(count.data[['mito']]))>0)
             NA
           else 
             100 * (colSums(count.data[['mito']]) / colSums(genes)))
  call.if.wanted('mito.plot', to.plot,
                 percmito=data,
                 logTxpts=logTxpts,
                 wellsets=wellsets,
                 style=style)
  
  call.if.wanted('txpt_vs_ercc.plot', to.plot,
                 logTxpts=logTxpts,
                 logERCCs=logERCCs,
                 wellsets=wellsets,
                 style=style,
                 livecell.cutoff=livecell.cutoff)
  
  call.if.wanted('goodwells.plot', to.plot,
                 ERCCs=goodERCCs,
                 wellsets=wellsets,
                 degraded.mode=degraded.mode)

  call.if.wanted('distribution.plot', to.plot,
                 logTxpts=logTxpts,
                 wellsets=wellsets,
                 livecell.cutoff=livecell.cutoff,
                 degraded.mode=degraded.mode)
  ## when running in fully degraded mode, this fails, but at least we have some sort of PDF
  
  call.if.wanted('cumulative.plot', to.plot,
                 gene.total=colSums(genes[,wellsets$fullgood]),
                 livecell.cutoff=livecell.cutoff)

  call.if.wanted('emptywells.plot', to.plot,
                 count.data=count.data,
                 wellsets=wellsets)

  call.if.wanted('genespertxpt.plot', to.plot,
                 logTxpts=logTxpts,
                 logGenes=log2(1+apply(genes,2,function(x)sum(x>0))),
                 wellsets=wellsets, style=style)

  ## three different plate plots:
  { perc.mapped <- calc.perc.mapped(stats, genes)
    call.if.wanted('rawperc_plate.plot', to.plot,
                   data=perc.mapped,
                   wellsets=wellsets)
    rm(perc.mapped)
  }
  
  { ticks <- (floor(min(logTxpts,na.rm=TRUE)):ceiling(max(logTxpts,na.rm=TRUE)))
    call.if.wanted('mappedtxpts_plate.plot', to.plot,
                   data=logTxpts,
                   ticks=ticks,
                   wellsets=wellsets)
    rm(ticks)
  }
  
  { ticks <-  -1:14
    logercc <- log2(colSums(erccs+1))
    call.if.wanted('ercc_plate.plot', to.plot,
                   data=logercc,
                   ticks=ticks,
                   wellsets=wellsets)
    rm(logercc)
  }
  
  if(is.null(saturations)) {
    for(type in c("genes", "umis") )
      warning("No accumulationplot for", type, "(too few reads?)")
  } else {
    type.name <- c(genes='gene', umis='UMI')
    linesperc <- args$accumulation_percentage
    subtitle <- ifelse(linesperc < 100, 
                       sprintf("first %d%% of reads", linesperc), "")
    
    ## over-all accumulation plots: gone since 26-Jan-2021
    
    ## per well accumulation plots:
    for(type in c("genes", "umis")) {
      main <- paste(type.name[type],  "accumulation per well")
      call.if.wanted("accumulation.plot", to.plot,
                     main=main, plot.id=paste0(type.name[type],"accu"),
                     subtitle=subtitle,
                     item.type=type.name[type],
                     saturations=saturations,
                     data=saturations$wellwise_mean[[type]],
                     ycol="mean",
                     diffdata=saturations$wellwise_mean_diff[[type]],
                     ycoldiff='diffofmean',
                     xlab="mapped reads seen", ylab=paste(type," per well"),
                     last.nreads=last.nreads,
                     ignored.message="")
      ## umis/gene gone since 23-Nov-2020
    }
  }
  
  call.if.wanted('complexity.plot', to.plot,
                 complexity=1+apply(genes[, wellsets$live],2,function(x)sum(x>0)))

  call.if.wanted('topexpression.plot', to.plot,
                 genes=genes[, wellsets$live])
  
  for (withinplate in c(TRUE, FALSE) ) 
    call.if.wanted('contamination.plot', to.plot,
                   genes=genes,
                   wellsets=wellsets,
                   within.plate=withinplate)
  if(!is.na(pdf))
    dev.off()
}                                       #platediagnostics

# Local variables:
# mode: R
# ess-indent-level: 2
# End:
