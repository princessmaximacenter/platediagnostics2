#!/bin/bash
# -*- mode: sh; -*-

warn(){ echo -e "$@" 1>&2; }
die(){ warn "$@"; exit 9; }

# This simple test script is just for testing the platediagnostics code
# in the current directory.  Simply execute it in the current test
# directory where it will analyze the data in testdata and produce
# output in testout (which is ignored by git).
#
# Variables that can be overriden:
# 
# onlyinstall=false
# onlyrun=false
# empties=col1
# inputdir=testdata # in this dir
# outputdir=testout # in this dir
# libloc=outputdir
#
# Note: due to the deliberately very limited data, the saturation plot
# code is not exercised. To get a more realistic test, consider running
# a different script, namely platediagnostics2-standalone.sh, like:
#
#   env empties=col1 outputdir=~/tmp  $GIT/sc-facility/miscUtilities/platediagnostics2-standalone.sh  DIR_WITH_ALL_DATA
#

versionfile=../R/version.R
echo "
### Overriding version file, created during testing
.git_package_version <- function() {
  return('branch TESTING-UNKNOWN, commit TESTING-UNKNOWN')
}
" > $versionfile

onlyinstall=${onlyinstall:false}
onlyrun=${onlyrun:false}

scriptdir=$(dirname $(realpath -e $BASH_SOURCE))

empties=${empties:-col1}
inputdir=${inputdir:-${scriptdir}/testdata}
outputdir=${outputdir:-${scriptdir}/testout}
libloc=${libloc:-${outputdir}}

set -euo pipefail

if [ -d $inputdir ] ; then
    warn "Using $inputdir for input"
else
    die "$inputdir is not a directory nor symlink to one"
fi

if [ -d $outputdir ] ; then
    warn "Found outpudir $outputdir, emptying it first"
    rm -fr $outputdir/* 2>/dev/null
else
    mkdir -p $outputdir
fi

stdout=$outputdir/stdout
stderr=$outputdir/stderr
touch $stdout $stderr

if [ x$onlyrun != xtrue ]; then

  R CMD INSTALL --library=$libloc ..
  rc=$? 
  if [ $rc  -ne 0  ]; then
      echo "Could not compile platediagnostics, exiting" >&2
      exit $rc
  fi
fi

if [ x$onlyinstall = xtrue ]; then
    exit $rc
fi

onlyplots="" # i.e. all the plots
## onlyplots="--plotOnly emptywells.plot,mito.plot"
### in order of appearance
## onlyplots='--plotOnly stats.plot' # or comma-separated, e.g.: mito.plot,emptywells.plot'
## onlyplots='--plotOnly mito.plot'
## onlyplots='--plotOnly txpt_vs_ercc.plot'
## onlyplots='--plotOnly distribution.plot'
## onlyplots='--plotOnly cumulative.plot'
## onlyplots='--plotOnly emptywells.plot'
## onlyplots='--plotOnly genespertxpt.plot'
## onlyplots='--plotOnly plate.plot' # this is actually three plots
## onlyplots='--plotOnly accumulation.plot'
## onlyplots='--plotOnly complexity.plot' # two of them
## onlyplots='--plotOnly topexpression.plot'
## onlyplots='--plotOnly contamination.plot'
 
script=$libloc/platediagnostics2/scripts/platediagnostics2.R # the recently installed one!

env R_LIBS=$libloc:$R_LIBS $script \
   --inputDir $inputdir \
   --inputs $inputdir/inputs.txt \
   --outputDir $outputdir \
   --outputPrefix testout \
   --empties $empties \
   --accumulation_percentage 90 \
   --ignore A4,B5,C6,D7,E8,F9 \
   --debug --verbose \
   $onlyplots 
               
#               \
#   --debug

rc=$? 
if [ $rc  -ne 0  ]; then
    echo "Exit status was $rc. See $stdout and/or $stderr" >&2
    exit $rc
fi

warnings=`realpath $outputdir/testout.warnings.txt`

## Put strings signifying serious errors (which may contain spaces but no
## wildcards) between the ZZZZZZZZZ. Any of them is sufficient to make
## the script exit with an error:
while IFS='' read msg; do 
   if grep -iq "$msg" $warnings $stdout $stderr; then
       echo "Found serious errors ('$msg'), see $warnings" >&2
       exit 1
   fi
done<<ZZZZZZZZ
error
not a multiple
non-  
closure
ZZZZZZZZ

echo -e "All OK\nAll output is in\n$outputdir\n(can be removed)"
## rm -fr ../testout

exit 0
