#!/bin/sh -x
# -*- mode: sh; -*-
warn ()  {  echo -e "$@" 1>&2; }
die ()  {  warn "$@"; exit 41; }

## create some very simplistic test data

cd ~/git/platediagnostics2/test/testdata
srcdir=/Users/philiplijnzaad/proj/facility/sharQC/PMSWR157SXE
## this is first lane of TM250,  empties is col1
## PMC-TM-250_HVFY2BGXB_S10_L001_R1_001.fastq.gz
## e.g. NB500901:154:HVFY2BGXB:1:11101:10000:7618       0       ERCC-00046      124     255     53M1I21M        *       0       0       GGTGGCAGTATGGGATTTCTAAAAATAATGTTAAGAATTTTTGCTGGTTTTTTTGCGACGTTGGTGTTGTAT

## *sat*.txt used to contain duplicates, check this
for i in *sat*; do
    if [ $(awk '/^[1-9]/{print $1}' $i | uniq -d | wc -l) -gt 0 ]; then
        die "duplicates in first column of $i"
    fi
done

for i in  $srcdir/*txt; do 
    head -1000 $i > ./$(basename $i)
done

bam=test.annotated.bam
samtools view -h $srcdir/$bam | head -2000 | samtools view  -O bam > $bam
