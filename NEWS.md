## NOTE: R package version 2.nnn corresponds to git tag 'v2nn'. I.e. drop the '2.' and prepend 'v'

# v209 (27-Jan-2021)

 * testdata now external

# v208 (26-Jan-2021)

 * backported all the stuff from platediagnostics1 since Nov 2020 (mostly saturation curve stuff)
 * no more reading/plotting of all-well saturations (only per well)
 * platediagnostics2_version() function automatically updated thru post-commit hook

# v207 (22-Jan-2021)

 * Now separate from Sharq2

